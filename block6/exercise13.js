let bankAccount =  {

  deposit: function(a){
      var result=a+bankAccount.total
      return bankAccount.total=result;
  },
  withdraw: function(a) {
      var result = bankAccount.total-a;
      return bankAccount.total=result;
  },
  balance: function() {
      return bankAccount.total;
  },
  total: 0
}

module.exports = {
    bankAccount
}