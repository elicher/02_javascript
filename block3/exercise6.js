var sum = (array) => {
  let summed = array.reduce((total, value) => total + value)
  return summed;
}
module.exports ={
    sum
}