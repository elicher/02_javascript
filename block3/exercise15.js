var longestString = (array, minLength) => {
  var result = array.filter((num) => num.length > minLength)
  result=result[result.length-1]; 
  return(result)
}

module.exports = {
    longestString
}
