var multy = (array) => {
  let multiplied = array.reduce((total, value) => total * value)
  return multiplied;
}
module.exports ={
	multy
}