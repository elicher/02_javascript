var assert = require("chai").assert;
var modifyObject = require("../block6/exercise3").modifyObject;

describe("#test1", function () {
  it("modifyObject should be a function", function () {
    assert.typeOf(modifyObject, "function");
  });
});
describe("#test2", function () {
  it(`modifyObject should be {c:22} `, function () {
    assert.equal(modifyObject({}, "c", 22).c, 22);
  });
});
describe("#test2", function () {
  it(`modifyObject should be {b: 33} `, function () {
    var myObj = modifyObject({}, "b", 33);
    assert.equal(myObj.b, 33);
  });
});
