var checker = (string) => {
    var commas = [];
    var questionMarks = [];
    for (var i=0;i<string.length;i++){
    if (string[i]==="?") {
    questionMarks.push(string[i]);
    } else if (string[i]===",") {
    commas.push(string[i]);
    }
    }
    if (commas.length>1){
    var comma="commas"
    } else {
    var comma="comma"
    }
    if(questionMarks.length>1) {
    var questionMark="question marks";
    } else {
    var questionMark="question mark";
    }
  var commasLength=commas.length;
  var questionMarksLength=questionMarks.length;
    return (`${commasLength} ${comma}, ${questionMarksLength} ${questionMark}`)
}

module.exports = {
    checker
}



