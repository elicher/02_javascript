
"=== EXERCISE 10: Guess My Number game' ==="
{/*

You are going to try to build a Guess My Number game with JavaScript

----------

The essence of this game is that computer randomly guesses some number and a player 
// has to guess it with limited number of guesses. 

----------
Requirements:
- game should generate a random number in a given range (1-100, for example)
- player should be told the range of possible numbers and how many guesses they have
- game should display a prompt with input field for a number
- after each guess game should tell:
- number is too big (if the number from user is greater than the guessed number)
- number is too small (if the number is smaller than the guessed number)
- If player could guess the number than the game should give a message about that and 
  render text/image confirming the win
- if player couldn’t guess the number with given amount of guesses then the game stops 
  and tells the number which was guessed
----------
Help
- Use built-in JavaScript method to create a random number
- Remember to define number of guesses first
- Use loop to repeat prompts asking user for a number
- Use conditionals to check if the numbers match
- the range, 0 - 10, if you put 100 it will be 1 - 100
----------

*/}
var guess_my_number = (attempts) => {
var number = Math.floor(Math.random() * 10) + 1;
var success = false;
var attempt = 0;
while (attempt<attempts&&success==false&&gameOver!=true) {
  var userNumber = window. prompt("Enter your number: ") 
  if (userNumber==number) {
  success=true
  } else {
  success=false
  attempt++
  }}
  if (success==false){
  var gameOver=true;
  success=null
  alert("YOU LOST.")} else {
    gameOver=true;
    alert("YOU WON!")
    success=null
  }}
var buba=3
guess_my_number(buba)