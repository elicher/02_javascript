var uniqueElements = (array) => {
    var unique = [];
    for (var i=0;i<array.length;i++) {
    if (!unique.includes(array[i])) {
    unique.push(array[i])
    }
    }
    return(`old array ${array}, new array ${unique}`);
}

module.exports = {
    uniqueElements
}