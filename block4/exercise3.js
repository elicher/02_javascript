var check_types = (array) => {
    var dataArray=[];
    for (var i = 0; i<array.length; i++) {
    var dataType=typeof(array[i]);
    if (!dataArray.includes(dataType)) {
    dataArray.push(dataType);
    }
    }
    return (dataArray.length)
}

module.exports ={
    check_types
}