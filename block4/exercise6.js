var numberConverter = (array) => {
var converted=[];
var unconvertible=[];
var numbers=[]
	for (var i = 0; i<array.length; i++){
    if (typeof(array[i])==="number") {
      numbers.push(array[i])
    }
 else if (array[i]===" "||isNaN(array[i])) {
unconvertible.push(array[i]);
	} else {
	converted.push(array[i])
	}
	}
	var convertedLength=converted.length;
  var unconvertibleLength=unconvertible.length;
  var numbersLength=numbers.length;
  if (numbersLength>0&&unconvertibleLength==0&&convertedLength==0) {
    return ("no need for conversion")
  } else {{
  if (convertedLength>1) {
    var toBe="were"
    var convertedWord="numbers"
  } else {
    var toBe="was"
    var convertedItem="number"
  }}
  return(`${convertedLength} ${toBe} converted to numbers: ${converted}, ${unconvertibleLength} couldn't be converted`)
}}

module.exports ={
	numberConverter
}

