var howManyCaps = (sentence) => {
    var capitalLetters=[];
    for (var i = 0; i<sentence.length; i++) {
    if (sentence[i]===sentence[i].toUpperCase()&&sentence[i]!==" ") {
    capitalLetters.push(sentence[i]);
    }
    if (capitalLetters.length>1) {
    var toBe="are";
    var capital="capitals"
    var theseAre="these are"
    } else {
  toBe="is";
    capital="capital"
    theseAre="this is"
    }}
    var capitalLettersLength=capitalLetters.length
  return(`There ${toBe} ${capitalLettersLength} ${capital} and ${theseAre} ${capitalLetters}`)
}
module.exports = {
    howManyCaps
}