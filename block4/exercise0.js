var isTrue = (data) => {
    return (data!==undefined&&data!==""&&data!==false&&data!==null&&data!==0&&typeof(data)!==NaN)
}
module.exports = {
    isTrue
}