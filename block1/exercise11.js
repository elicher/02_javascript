function toFahr(degCelcius){
    return Math.round(((degCelcius/5*9)+32));
}
function toCelsius(degFahr){
    return Math.round(((degFahr-32)*5/9));
}

module.exports = {
    toCelsius, 
    toFahr,
}