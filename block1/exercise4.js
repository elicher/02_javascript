function howManyTeas(currentAge,deathAge,teasPerDay){
    return (deathAge-currentAge)*(365*teasPerDay)
}

module.exports = {
    howManyTeas
}