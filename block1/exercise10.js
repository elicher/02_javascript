function daysInYear(year){
  if(year%4===0){
    return 366;
  }
  else {
    return 365;
  }
}
function howManyDays(yearOfBirth,currentYear){
  var years=currentYear-yearOfBirth;
  var leapYears=Math.round(years/4)
  var nonLeapYears=years-leapYears;
  var days=nonLeapYears*365+leapYears*366;
  return "you have lived for "+days+" days already!"
}
module.exports = {
    howManyDays
}