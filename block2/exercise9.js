var concatenator = (array1,array2) => {
  var newArray = array1.concat(array2);
  return newArray;
}

module.exports = {
    concatenator,
}
