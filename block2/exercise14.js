var isArrayFunc = (arg) => {
    return Array.isArray(arg);
}


module.exports = {
    isArrayFunc
}
