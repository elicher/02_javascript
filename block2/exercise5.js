var removeFirstAndLast = (array) => {
    array.pop();
    array.shift();
    return array;
}


module.exports = {
    removeFirstAndLast
}