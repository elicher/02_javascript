var arr = ["banana", "apple", "orange"];
var arr2 = ["tv", "dvd-player", "playstation"];
var swap = (array1, array2, number) => {
	var newArray=[]
  var initialArrayItem=array1[number];
  array1[number]=array2[number];
  array2[number]=initialArrayItem;
  newArray.push(array1)
  newArray.push(array2)
  return(newArray);
}


module.exports = {
	swap,
}
