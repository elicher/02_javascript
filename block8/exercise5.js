function sorter(array,order="ascending") {
  var done = false;
  if (order==="ascending") {
  while (!done) {
    done = true;
    for (var i = 1; i < array.length; i++) {
      if (array[i - 1] > array[i]) {
        done = false;
        var swapper = array[i - 1];
        array[i - 1] = array[i];
        array[i] = swapper;
      }
    }
  } return array;}
  if (order==="descending") {
      while (!done) {
    done = true;
    for (var i = 1; i < array.length; i++) {
      if (array[i - 1] < array[i]) {
        done = false;
        var swapper = array[i - 1];
        array[i - 1] = array[i];
        array[i] = swapper;
      }
    }
  }
  return array; }
  if (order!=="ascending"&&order!=="descending"){
    return(`wrong order provided ${order} please provide us with ascending or descending order instructions`)
  }
}

module.exports ={
    sorter
}