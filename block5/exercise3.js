var filter =  (array,dataType,minLength) => {
  var newArray = []
  for (var i=0; i<array.length; i++) {
  if (typeof(array[i])!==dataType&&(array[i].length)>=minLength) {
  newArray.push(array[i])
  }
  }
  return(newArray)
}

module.exports = {
    filter
}