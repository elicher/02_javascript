var tellAge = (uday,umonth,uyear) => {
  var today =  new Date();
  var todayMilliseconds = today.getTime(); 
  var birthday = (`${uyear}/${umonth}/${uday}`);
  birthday = new Date(birthday);
  var birthdayMilliseconds = birthday.getTime();
  var calculation = todayMilliseconds - birthdayMilliseconds;
  var second = Math.floor(calculation / 1000);
  var minute = Math.floor(second / 60);
   second = second % 60;
  var hour = Math.floor(minute / 60);
   minute = minute % 60;
  var day = Math.floor(hour / 24);
   hour = hour % 24;
  var month = Math.floor(day / 30);
   day = day % 30;
  var year = Math.floor(month / 12);
   month = month % 12;
   day = month*day;
if (calculation<0) {
  return("You can't be born in future")
}
else if (calculation<31536000000) {
  return(`You are ${day} days old`)
}
else if (calculation>31536000000) {
  return (`You are ${year} years old`)
}
}

module.exports = {
    tellAge
}