var stringChop = (string,len=0) => {
  var arr = [];
  if (len=="undefined"||typeof(len)!=="number"||len===0){
    arr.push(string)
    return(arr)
  } else {
  for (var i = 0; i<string.length; i+=len){
  arr.push(string.substring(i,(i+len)))
  }
  return arr
}}

module.exports = {
    stringChop
}