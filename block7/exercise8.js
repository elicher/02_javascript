let categories = [
  {id: 1, catName: 'shoes'},
  {id: 2, catName: 'hats'}
];
let products = [
  {name:'black shoes', catID:1},
  {name:'black hat', catID:2}
  ];


class shop  {
   constructor(categories,products){
      this.categories = categories;
      this.products = products;
  }
  /// actions ///
  addNewProduct(productName,catName){
    /// creating objects ///
    class createNewProduct {
        constructor() {
        this.name = productName
        this.catID = categories.findIndex((element) => element.catName === catName)+1
    }
      }
    class createNewCategory {
        constructor() {
        this.id = categories.length+1
        this.catName = catName
    }

    ///creating variables for this action///

      }
      var productIndex=((this.products.findIndex((element)=>element.name==productName)))
      var catIndex=(this.categories.findIndex((element)=>element.catName==catName))

     //////////

      if (productIndex>-1){
        console.log("this product has already been added to the database")
        return("this product has already been added to the database")
      }
      if (catIndex>-1) {
        console.log("there is such category")
      var newProduct = new createNewProduct
      products.push(newProduct)
      console.log(`product ${productName} has been added to ${catName}`)
  } else if (catIndex==-1) {
      var newCategory= new createNewCategory
      categories.push(newCategory)
      var newProduct = new createNewProduct
      products.push(newProduct)
      console.log(`NEW category ${catName} was created and product ${productName} was added successfully`)
  }
  }
  renameCat(catName,newCatName){
    var catIndex=(this.categories.findIndex((element)=>element.catName==catName))
    if(catIndex>-1){
    categories[catIndex].catName=newCatName
    console.log(categories)
  } else {
    console.log(`Category ${catName} doesn't exist`)
  }
}
displayAll(catName){
  if (catName==undefined){
    categories.forEach(function (category) {
    console.log(`${category.catName} contains following items:`);
    products.forEach(function(product){
      if (product.catID==category.id) {
        console.log(`${product.name}`)
      }
    })
})
    }
  else {
  var allCatProducts=[]
  var catIndex=(this.categories.findIndex((element)=>element.catName==catName))
  for (var key in products){
    if (products[key].catID===catIndex+1){
      allCatProducts.push(products[key].name)
    }
    }
  console.log(`Category ${catName} contains ${allCatProducts}.`)
  return (allCatProducts)
}}
}
module.exports = {
  addNewProduct, renameCat, displayAll, categories, products
}