var DB = [
    {
        genre:'thriller', 
        movies: [
            {
                title:'the usual suspects', release_date:1999
            }
        ]
    },
    {
        genre:'comedy', 
        movies: [
            {
                title:'pinapple express', release_date:2008
            }
        ]
    }
]

var moviesDB = (DB, genre, object) => {
  var checker=(DB.findIndex((element) => element.genre === genre))

  if (checker>-1) {
    console.log(DB[checker].movies)
    var checker2 = (DB[checker].movies.findIndex((element) => element.title == object.title))
    if (checker2>-1) {
      return("this movie is already in the DB")
    } else {
      DB[checker].movies.push(object)
    }
  } 
  else {
    DB.push({genre: genre, movies: [{title: object.title}]})
  }
  return (DB)}
console.log(moviesDB(DB, 'comedy', {title:'Banana'}))
console.log(moviesDB(DB, 'thriller', {title:'the usual suspects'}))

module.exports ={
	moviesDB
}