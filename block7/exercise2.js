var addCurrency = (coinobj,amount,database) => {
  var index=database.findIndex((element)=>element.coin === coinobj.coin)
  function capitalize(string) {
  return (string.charAt(0).toUpperCase() + string.slice(1))
}
  if (index>=0) {

function findCurrency(coinobj,amount,index,database) {
  var result = (database[index].rate)
  var coinname = (database[index].coin)
  converter(result,amount,coinname)
}
function converter(result,amount,coinname) {
  result=result*amount
  tellRate(result,amount,coinname)
}
function tellRate(result,amount,coinname) {
  coinname=capitalize(coinname)
  var string=(`You will receive ${result} dollars for ${amount} ${coinname}s`)
  return(string)
}

findCurrency(coinobj,amount,index,database)

}
  
else {
    database.push(coinobj)
    var coinname=capitalize(coinobj.coin)
    var string=(`New coin ${coinname} added to the Database`)
    console.log(string)
    return (string)
  }
}

module.exports = {
  addCurrency,
  findCurrency,
  converter,
  tellConversion,
};
