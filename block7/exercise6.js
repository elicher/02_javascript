let perfectFamily = {
    father:{ name:'Mike', age:44, height:179 },
    mother:{ name:'Jenny', age:40, height:168 },
    son:{ name:'Pablo', age:16, height:165 }
}

let otherFamilies = [
    {Smiths:{
        father:{ name:'Jake', age:38, height:182 },
        mother:{ name:'Viola', age:36, height:172 },
        son:{ name:'Donny', age:14, height:180 }
        }
    },
    {Morenos:{
        father:{ name:'Juan', age:42, height:188 },
        daughter:{ name:'Julia', age:10, height:149 },
        mother:{ name:'Kate', age:36, height:172 }
            }
    },
    {Tanakas:{
        father:{ name:'Kioto', age:39, height:172 },
        mother:{ name:'Junko', age:42, height:164 },
        son:{ name:'Bundo', age:24, height:164 }
            }
    }
]

var familyAffairs = (otherFamilies) => {
    var kids=[]
for (var key in otherFamilies){
if (!Object.values(otherFamilies[key])[0]?.mother){
            return (`Yay! Jenny moved to ${Object.keys(otherFamilies[key])}`);
}}
for (var key in otherFamilies){
if (!Object.values(otherFamilies[key])[0]?.son&&!Object.values(otherFamilies[key])[0]?.daughter){
            return (`Yay! Jenny moved to ${Object.keys(otherFamilies[key])}`);
}
}
for (var key in otherFamilies){
if (Object.values(otherFamilies[key])[0]?.daughter){
kids.push(Object.values(otherFamilies[key])[0].daughter)
}
}
for (var key in otherFamilies){
if (Object.values(otherFamilies[key])[0]?.son){
kids.push(Object.values(otherFamilies[key])[0].son)
}
}
var kidsage=[]
for (var key in kids) {
  kidsage[key]=kids[key].age
}
var maxAge=Math.max.apply(null, kidsage)
var index=(kids.findIndex((element) => element.age === maxAge))
return (`Yay! Jenny moved to ${Object.keys(otherFamilies[index])}`)
}

console.log(familyAffairs(otherFamilies))

module.exports = {
	familyAffairs
}
