class BankAccount  {
   constructor(initial = 0){
      this.total = initial;
  }
  deposit(num){
      this.total += num;
  }
  withdraw(num) {
      this.total -= num;
  }
  balance () {
      console.log(this.total)
      return this.total;
  }
}
module.exports = {
    BankAccount
}
